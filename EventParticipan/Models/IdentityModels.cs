﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EventParticipan.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>

    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
            //: base("MY_CONNECTION_STRING")

        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Participan> Participans { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // configures one-to-many relationship

            //modelBuilder.Entity<Participan>()
            //    .HasRequired<Event>(s => s.CurrentEvent)
            //    .WithMany(g => g.Participans)
            //    .HasForeignKey<int>(s => s.CurrentEventId);

            modelBuilder.Entity<Event>()
                .HasMany<Participan>(g => g.Participans)
                .WithRequired(s => s.CurrentEvent)
                .HasForeignKey<int?>(s => s.CurrentEventId)
                .WillCascadeOnDelete();
        }
    }
}
