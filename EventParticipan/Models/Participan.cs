﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventParticipan.Models
{
    public enum ClientType {[Display(Name = "Eraisik")] PrivatePerson, [Display(Name = "Firma või asutus")] JuridicalPerson }
    public enum PaymentMethod {[Display(Name = "Pangaülekanne")] BankTransfer, [Display(Name = "Sularaha")] Cash }

    public class Participan
    {
        public ClientType ClientType { get; set; }

        public int Id { get; set; }

        [Display(Name = "Firma nimi")]
        public string LegalName { get; set; }

        [Display(Name = "Registrinumber")]
        public Int64? RegistryNumber { get; set; }

        [Display(Name = "Osalejate arv")]
        public int? NrOfParticipans { get; set; }

        [Display(Name = "Maksmisviis")]
        public PaymentMethod PaymentMethod { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(5000)]
        [Display(Name = "Lisainfo")]
        public string JuridicalClientText { get; set; }

        [Display(Name = "Eesnimi")]
        public string FirstName { get; set; }

        [Display(Name = "Perenimi")]
        public string LastName { get; set; }

        [Display(Name = "Isikukood")]
        public Int64? IdentityCode { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(1500)]
        [Display(Name = "Lisainfo")]
        public string PrivateClientText { get; set; }

        
        public int CurrentEventId { get; set; }

        public Event CurrentEvent { get; set; }

        public string FullName => $"{FirstName} {LastName}";
    }
}