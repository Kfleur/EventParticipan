﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventParticipan.Models
{
    public class EventParticipans
    {
        public Event Event { get; set; }
        public Participan Participan { get; set; }
        public ICollection<Participan> Participans { get; set; }
        //public ICollection<Event> Events { get; set; }

    }
}