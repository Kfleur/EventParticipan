﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventParticipan.Models
{
    public class Event
    {
        public int EventId { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli!")]
        [Display(Name = "Ürituse nimi")]
        public string EventName { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli!")]
        [Display(Name = "Toimumisaeg")]
        [DataType(DataType.Date)]
        public DateTime EventTime { get; set; }

        [Required(ErrorMessage = "Kohustuslik väli!")]
        [Display(Name = "Toimumise koht")]
        public string EventPlace { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Lisainfo")]
        [StringLength(1000)]
        public string EventText { get; set; }

        public ICollection<Participan> Participans { get; set; }
    }
}