﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EventParticipan.Models;

namespace EventParticipan.Controllers
{
    public class ParticipansController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Participans
        public ActionResult Index(int? id)
        {
            //Event @event = new Event();
           // @event = db.Events.Find(@event.EventId);
            //db.Entry(@event).Collection(p => p.Participans).Load();

            //foreach (var participan in @event.Participans)
            //{
            //    @event = db.Events
            //            .Where(b => b.EventId == participan.CurrentEventId)
            //            .Include(b => b.Participans)
            //            .FirstOrDefault();
            //}

            var participans = db.Participans.Include(c => c.CurrentEvent)/*.Where(x => x.CurrentEventId == @event.EventId)*/;
            return View(participans.ToList());
           
        }

        // GET: Participans/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participan participan = db.Participans.Find(id);
            if (participan == null)
            {
                return HttpNotFound();
            }
            return View(participan);
        }

        // GET: Participans/Create
        public ActionResult Create(int? currentEventId)
        {

            ViewBag.CurrentEventId = new SelectList(db.Events, "EventId", "EventName");
            return View();
        }

        // POST: Participans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ClientType,LegalName,RegistryNumber, Participans,PaymentMethod,JuridicalClientText,FirstName,LastName,IdentityCode,PrivateClientText,CurrentEventId")] Participan participan)
        {
            EventParticipans eventParticipans = new EventParticipans();
            ViewBag.CurrentEventId = new SelectList(db.Events, "EventId", "EventName", participan.CurrentEventId);
            if (ModelState.IsValid)
            {
                db.Participans.Add(participan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eventParticipans);
        }

        // GET: Participans/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participan participan = db.Participans.Find(id);
            if (participan == null)
            {
                return HttpNotFound();
            }
            ViewBag.CurrentEventId = new SelectList(db.Events, "EventId", "EventName", participan.CurrentEventId);
            return View(participan);
        }

        // POST: Participans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ClientType,LegalName,RegistryNumber,Participans,PaymentMethod,JuridicalClientText,FirstName,LastName,IdentityCode,PrivateClientText,CurrentEventId")] Participan participan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(participan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CurrentEventId = new SelectList(db.Events, "EventId", "EventName", participan.CurrentEventId);
            return View(participan);
        }

        // GET: Participans/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participan participan = db.Participans.Find(id);
            if (participan == null)
            {
                return HttpNotFound();
            }
            return View(participan);
        }

        // POST: Participans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Participan participan = db.Participans.Find(id);
            db.Participans.Remove(participan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
