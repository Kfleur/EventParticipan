﻿using EventParticipan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventParticipan.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            Event @event = new Event();
            
            if (@event.EventTime >= DateTime.Now)
            {
                return View(db.Events.OrderByDescending(x => x.EventTime).ToList());
            }
            return View(db.Events.ToList());
            
        }

        public ActionResult ShowEventWithParticipans()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}